package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"html/template"
	"log"
	"net/http"
	"strings"
)

func sayhelloName(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	// ^^^^ You need this, otherwise you can't get the data on the next few lines from the webpage request --- this is useful for logging and debugging
	fmt.Println(r.Form) //The next few lines are printing the information about the request information on the server to be used if needed. Something like this would not be useful in production
	fmt.Println("path", r.URL.Path)
	fmt.Println("scheme", r.URL.Scheme)
	fmt.Println(r.Form["url_long"])
	for k, v := range r.Form {
		fmt.Println("key:", k)
		fmt.Println("val:", strings.Join(v, ""))
	}
	fmt.Fprintf(w, "HI PLANET")
}

type UserLogin struct {
	Uname string `json:"username"`
	Pword string `json:"password"`
}

func login(w http.ResponseWriter, r *http.Request) {

	fmt.Println("method:", r.Method) //get request method (either GET or POST. This is tells us if it the initial page request (a GET) or the form submission (which is a POST))
	if r.Method == "GET" {
		t, _ := template.ParseFiles("login.gtpl")
		t.Execute(w, nil) // These 2 lines call the login page I created in HTML, prepping it to be shown (ParseFiles), then executing the page (showing it)
	} else {
		// r.ParseForm()
		//
		// Else means the page has sent a POST request, meaning they submitted the login form (Username and Password). ParseForm is getting the data from the form
		// so we can use it in code. This is where the logic would go to actually check and verify whether or not the username and password actually matches what is in the db
		// Will write this eventually. For now, we're just going to print the username and password on the server side and not do anything with it.
		//

		// testUser := UserLogin{(strings.Join(r.Form["username"], "")), (strings.Join(r.Form["password"], ""))}

		// data, _ := json.MarshalIndent(testUser, "", "\t")

		// w.Header().Add("Content-Type", "application/json")

		// w.WriteHeader(200)

		// w.Write(data)

		var testUser UserLogin

		decoder := json.NewDecoder(r.Body)
		decoder.Decode(&testUser)

		fmt.Println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@: ", testUser)

		// testUser.Uname = strings.Join(r.Form["username"], "")
		// testUser.Pword = strings.Join(r.Form["password"], "")

		// fmt.Println("username:", r.Form["username"])
		// Uname = strings.Join(r.Form["username"], "")

		// fmt.Println("password:", r.Form["password"])
		// Pword = strings.Join(r.Form["password"], "")

		db, err := sql.Open("mysql", "root:in0Devtech!@/test_database")
		checkErr(err)

		stmt, err := db.Prepare("INSERT users SET username=?, first_name=?, last_name=NULL, email=NULL")
		checkErr(err)

		res, err := stmt.Exec(testUser.Uname, testUser.Pword)
		checkErr(err)

		id, err := res.LastInsertId()
		checkErr(err)

		fmt.Println(id)

	}
}

func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}

func main() {
	http.HandleFunc("/", sayhelloName)     //the routing rule ie: just the address HOSTNAME/ (with no additional page request)
	http.HandleFunc("/login", login)       //the routing rule ie: HOSTNAME/login
	err := http.ListenAndServe(":80", nil) //sets the listening port, default 80 for web server
	if err != nil {
		log.Fatal("ListenAndServe: ", err) //in the line above, ListenAndServe is probably the most imporant method in this code block, it runs on loop until something actually hits
		//the port we set (80) and then triggers everything else in main. If that fails (if there is an error anywhere), this log.Fatal prints on
		//the server side what the error is (variable err)
	}
}
