package main

import (
	// "encoding/json"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"strings"
)

func render(w http.ResponseWriter, tmpl string) {

	tmpl = fmt.Sprintf("./%s", tmpl)    // stores file (directed by r.URL.path) into tmpl
	t, err := template.ParseFiles(tmpl) // parses the file into a renderable template
	if err != nil {
		log.Print("template parsing error: ", err)
	}
	err = t.Execute(w, "") // executes the Response as the rendered template
	if err != nil {
		log.Print("template executing error: ", err)
	}
}

func defaultServe(w http.ResponseWriter, r *http.Request) {
	path := r.URL.Path
	rtype := r.Header.Get("Content Type")
	// var contentType string = ""
	if path == "/" {
		path += "index.html"
		render(w, path) // if url is blank, appends index.html to r.URL.Path before passing to renderer
	} else {
		if strings.HasSuffix(path, ".css") {
			// contentType = "text/css"
			w.Header().Set("Content Type", "text/css")
		}
		log.Printf(rtype)
		render(w, path) //grabs url path from Request, passes url string to render to be parsed into renderable template
	}
}

func main() {
	http.HandleFunc("/", defaultServe)       //the routing rule ie: just the address HOSTNAME/ (with no additional page request)
	err := http.ListenAndServe(":8080", nil) //sets the listening port
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}
